# simcax.dk - website in HUGO
This repo contains and builds the code for [simcax.dk](https://www.simcax.dk) - a personal website with a blog about my home improvement projects mostly.

# HUGO
The blog is built with [HUGO](https://gohugo.io/), and is written in Markdown. 
